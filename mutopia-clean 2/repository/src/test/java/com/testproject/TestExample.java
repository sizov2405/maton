package com.testproject;

import com.testproject.repository.config.H2Config;
import com.testproject.repository.config.PropertyLoaderConfig;
import com.testproject.repository.model.entity.Child;
import com.testproject.repository.persistence.dao.ChildDao;
import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;


/**
* Created by aleksandrsizov on 07.02.15.
*/
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {H2Config.class, PropertyLoaderConfig. class})
@TransactionConfiguration(defaultRollback = true)
@Transactional
@ActiveProfiles("h2")
public class TestExample {

    @Autowired
    ChildDao childDao;

    @Test
    public void testExampleMethod(){
        Child child = new Child();
        child.setText("a");
        long id = (long)childDao.save(child);

        Child fromDataBase = childDao.findOne(id);
        Assert.assertTrue(child.getText().equals(fromDataBase.getText()));
    }
}
