package com.testproject.repository.persistence.dao;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;

public interface Operations<T extends Serializable> {

    T findOne(final long id);

    List<T> findAll();

    @Transactional
    void saveOrUpdate(final T entity);

    @Transactional
    void delete(final T entity);

    @Transactional
    Serializable save(final T entity);

    @Transactional
    void saveAll(List<T> entities);

}
