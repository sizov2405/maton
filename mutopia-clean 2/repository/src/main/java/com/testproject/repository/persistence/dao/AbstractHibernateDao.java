package com.testproject.repository.persistence.dao;

import com.google.common.base.Preconditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("unchecked")
public abstract class AbstractHibernateDao<T extends Serializable> implements Operations<T> {

    private Class<T> clazz;

    @Autowired
    protected HibernateTemplate hibernateTemplate;

    // API

    protected void setClazz(final Class<T> clazzToSet) {
        clazz = Preconditions.checkNotNull(clazzToSet);
    }

    @Override
    public T findOne(final long id) {
        return (T) hibernateTemplate.get(clazz, id);
    }

    @Override
    public List<T> findAll() {
        return hibernateTemplate.loadAll(clazz);
    }

    public Serializable save(final T entity){
        Preconditions.checkNotNull(entity);
        return hibernateTemplate.save(entity);
    }

    @Override
    public void saveOrUpdate(final T entity) {
        Preconditions.checkNotNull(entity);
        hibernateTemplate.saveOrUpdate(entity);
    }

    @Override
    public final void delete(final T entity) {
        Preconditions.checkNotNull(entity);
        hibernateTemplate.delete(entity);
    }

    @Override
    public void saveAll(List<T> entities){
       Preconditions.checkState(entities != null && !entities.isEmpty());
       for(T entry : entities){
           hibernateTemplate.save(entry);
       }
    }
}
