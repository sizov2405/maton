package com.testproject.repository.persistence.dao;

import com.testproject.repository.model.entity.Child;

public interface ChildDao extends Operations<Child> {
}
