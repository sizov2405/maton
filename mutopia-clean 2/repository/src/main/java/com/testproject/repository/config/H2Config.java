package com.testproject.repository.config;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

/**
* Created by aleksandrsizov on 12.02.15.
*/

/**
 * In memory H2-database config. Used only for test purposes
 */
@Configuration
@Profile("h2")
@EnableTransactionManagement
@PropertySource({ "classpath:persistence.properties" })
@ComponentScan({ "com.testproject.repository" })
public class H2Config implements RepositoryConfig {

    private static final String LAUNCH_SCRIPT = "create-database.sql";

    @Bean
    @Override
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean result = new LocalSessionFactoryBean();
        result.setDataSource(dataSource());
        result.setPackagesToScan(new String[] { "com.testproject.repository" });
        Properties properties = new Properties();
        properties.setProperty("hibernate.hbm2ddl.auto", "create-drop");
        result.setHibernateProperties(properties);
        return result;
    }

    @Bean
    @Override
    public DataSource dataSource() {
        return new EmbeddedDatabaseBuilder().setType(EmbeddedDatabaseType.H2)
                .setName("test").addScript(LAUNCH_SCRIPT).build();
    }

    @Bean
    @Override
    public HibernateTransactionManager transactionManager() {
        HibernateTransactionManager man = new HibernateTransactionManager();
        man.setSessionFactory(sessionFactory().getObject());
        return man;
    }

    @Bean
    @Autowired
    public HibernateTemplate hibernateTemplate(SessionFactory s){
        return new HibernateTemplate(s);
    }
}
