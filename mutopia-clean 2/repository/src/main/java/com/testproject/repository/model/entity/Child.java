package com.testproject.repository.model.entity;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

//test entity
@Entity
@Table(name = Child.TABLE_NAME)
public class Child implements Serializable {

    @Id
    @GeneratedValue
    protected long id;
    //Required field for each entity
    public static final String TABLE_NAME = "child";

    private String text;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Child)) return false;

        Child child = (Child) o;

        if (id != child.id) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

}
