package com.testproject.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by elena on 27.02.15.
 */
public class Test {

    public static void main(String [] args) throws ClassNotFoundException {
        try {
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/luxoft?autoReconnect=true&autoReconnectForPools=true", "root", "root");
            System.out.print("ok");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
