package com.testproject.repository.persistence.dao.impl;

import com.testproject.repository.model.entity.Parent;
import com.testproject.repository.persistence.dao.AbstractHibernateDao;
import com.testproject.repository.persistence.dao.ParentDAO;
import org.springframework.stereotype.Repository;

/**
 * Created by aleksandrsizov on 14.04.16.
 */
@Repository
public class ParentDaoImpl extends AbstractHibernateDao<Parent> implements ParentDAO {

    public ParentDaoImpl(){setClazz(Parent.class);}

}
