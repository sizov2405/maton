package com.testproject.repository.persistence.dao.impl;

import com.testproject.repository.model.entity.Child;
import com.testproject.repository.persistence.dao.AbstractHibernateDao;
import com.testproject.repository.persistence.dao.ChildDao;
import org.springframework.stereotype.Repository;

/**
 * Created by aleksandrsizov on 13.02.15.
 */
@Repository
public class ChildDaoImpl extends AbstractHibernateDao<Child> implements ChildDao {

    public ChildDaoImpl(){setClazz(Child.class);}

}
