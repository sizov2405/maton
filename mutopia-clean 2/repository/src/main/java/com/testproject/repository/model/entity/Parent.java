package com.testproject.repository.model.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by aleksandrsizov on 14.04.16.
 */

@Entity
public class Parent implements Serializable {

    @Id
    @GeneratedValue
    long id;

    @OneToMany
    @JoinColumn(name="PARENT_ID")
    List<Child> children = new ArrayList();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<Child> getChildren() {
        return children;
    }

    public void setChildren(List<Child> children) {
        this.children = children;
    }
}
