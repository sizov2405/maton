package com.testproject.service.impl;

import com.testproject.repository.model.entity.Child;
import com.testproject.repository.model.entity.Parent;
import com.testproject.repository.persistence.dao.ChildDao;
import com.testproject.repository.persistence.dao.ParentDAO;
import com.testproject.service.ExampleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by aleksandrsizov on 09.03.15.
 */
@Service
public class ExampleServiceImpl implements ExampleService {

    @Autowired
    ChildDao childDao;

    @Autowired
    ParentDAO parentDAO;

    @Override
    public String saveParentWithChildren(Parent parent, List<Child> children) {
//        Child child1 = new Child();
//        child1.setText("child1");
//        childDao.save(child1);
//
//        Child child2 = new Child();
//        child2.setText("child2");
//        childDao.save(child2);
//
//        List<Child> children = new ArrayList<>();
//        children.add(child1);
//        children.add(child2);
//
//        Parent parent = new Parent();
//        parent.setChildren(children);
        childDao.saveAll(children);

        parent.setChildren(children);
        parentDAO.save(parent);

        return "";
    }
}
