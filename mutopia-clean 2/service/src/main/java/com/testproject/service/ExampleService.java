package com.testproject.service;

import com.testproject.repository.model.entity.Child;
import com.testproject.repository.model.entity.Parent;

import java.util.List;

/**
 * Created by aleksandrsizov on 09.03.15.
 */
public interface ExampleService {

    String saveParentWithChildren(Parent parent, List<Child> children);
}
