package com.testproject.view.controller;

import com.testproject.repository.model.entity.Child;
import com.testproject.repository.model.entity.Parent;
import com.testproject.service.ExampleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;




@Controller
public class AppController {

    @Autowired
    ExampleService exampleService;

    /**
	 * example method for JSP mapping
	 */
	@RequestMapping(value = "/example" , method = RequestMethod.GET)
	public String listUsers(ModelMap model) {
//                Child child1 = new Child();
//        child1.setText("child1");
//
//        Child child2 = new Child();
//        child2.setText("child2");
//
//        List<Child> children = new ArrayList<>();
//        children.add(child1);
//        children.add(child2);
//
//        Parent parent = new Parent();
//        parent.setChildren(children);
//
//
//        exampleService.saveParentWithChildren(parent, children);
		return "example";
	}
}
